//
//  TVSNetworkController.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSNetworkController.h"
#import "AFHTTPSessionManager.h"



@interface TVSNetworkController()

@property NSURL *baseURL;
@property AFHTTPSessionManager *networkManager;

@end


@implementation TVSNetworkController

+ (instancetype)sharedInstance
{
    static TVSNetworkController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance commonInit];
    });
    return sharedInstance;
}

+ (NSString *)baseURL;
{
    return @"";
}

-(void)commonInit{
    NSString *urlStr = [[self class] baseURL];
    self.baseURL = [NSURL URLWithString:urlStr];
    self.networkManager = [[AFHTTPSessionManager alloc] initWithBaseURL:self.baseURL];
    self.networkManager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.networkManager.responseSerializer = [AFJSONResponseSerializer serializer];
}


-(void)getRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    [self getRequestWithPath:path params:nil completionBlock:completionBlock];
}

-(void)putRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    [self putRequestWithPath:path params:nil completionBlock:completionBlock];
}

-(void)postRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    [self postRequestWithPath:path params:nil completionBlock:completionBlock];
}

-(void)deleteRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    [self deleteRequestWithPath:path params:nil completionBlock:completionBlock];
}

-(void)getRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    
    [self.networkManager GET:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if(completionBlock) {
            completionBlock(responseObject, nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if(completionBlock) {
            completionBlock(nil, error);
        }
    }];
}

-(void)putRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    
    [self.networkManager PUT:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if(completionBlock) {
            completionBlock(responseObject, nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if(completionBlock) {
            completionBlock(nil, error);
        }
    }];
    
}

-(void)postRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    
    [self.networkManager POST:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if(completionBlock) {
            completionBlock(responseObject, nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if(completionBlock) {
            completionBlock(nil, error);
        }
    }];
    
}

-(void)deleteRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock{
    
    [self.networkManager DELETE:path parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if(completionBlock){
            completionBlock(responseObject, nil);
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if(completionBlock) {
            completionBlock(nil, error);
        }
    }];
    
}


-(void)cancelAllRequests{
    [self.networkManager.operationQueue cancelAllOperations];
}
@end

