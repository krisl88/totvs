//
//  TVSNetworkController.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVSNetworkController : NSObject

typedef void (^TVSNetworkCompletionBlock)(id responseObject, NSError* error);
typedef void (^TVSErrorCompletionBlock)(NSError* error);
typedef void (^TVSObjectCompletionBlock)(id mappedObject, NSError* error);
typedef void (^TVSArrayCompletionBlock)(NSArray *mappedObjects, NSError* error);
typedef void (^TVSDictionaryCompletionBlock)(NSDictionary *mappedObjects, NSError* error);

+(instancetype)sharedInstance;


-(void)getRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock;
-(void)putRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock;
-(void)postRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock;
-(void)deleteRequestWithPath:(NSString*)path completionBlock:(TVSNetworkCompletionBlock)completionBlock;

-(void)getRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock;
-(void)putRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock;
-(void)postRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock;
-(void)deleteRequestWithPath:(NSString*)path params:(NSDictionary*)params completionBlock:(TVSNetworkCompletionBlock)completionBlock;

-(void)cancelAllRequests;
@end
