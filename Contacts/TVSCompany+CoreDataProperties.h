//
//  TVSCompany+CoreDataProperties.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSCompany.h"

NS_ASSUME_NONNULL_BEGIN

@interface TVSCompany (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *location;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) TVSContact *contact;

@end

NS_ASSUME_NONNULL_END
