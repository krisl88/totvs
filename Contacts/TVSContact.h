//
//  TVSContact.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVSManagedObject.h"

@class TVSAddress, TVSCompany, TVSEmail, TVSPhone;

NS_ASSUME_NONNULL_BEGIN

@interface TVSContact : TVSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "TVSContact+CoreDataProperties.h"
