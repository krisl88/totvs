//
//  TVSEmail.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSEmail.h"
#import "TVSContact.h"
#import "TVSDataController.h"

@implementation TVSEmail

// Insert code here to add functionality to your managed object subclass
+(NSSet*)setFromArray:(NSArray *)array{
    NSMutableSet *set = [NSMutableSet new];
    for(NSDictionary *dictionary in array)
    {
        TVSEmail *email = [TVSEmail findOrCreateWithUniqueValue:dictionary[@"email"] forKey:@"email" inManagedObjectContext:[TVSDataController sharedInstance].mainManagedObjectContext];
        [email updateWithResponseDictionary:dictionary];
        [set addObject:email];
    }
    return set;
}

-(void)updateWithResponseDictionary:(NSDictionary *)dictionary{
    self.label = dictionary[@"label"];
}

@end
