//
//  TVSCompany+CoreDataProperties.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSCompany+CoreDataProperties.h"

@implementation TVSCompany (CoreDataProperties)

@dynamic location;
@dynamic name;
@dynamic contact;

@end
