//
//  TVSContactCell.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSContactCell.h"
#import "TVSContact.h"

@implementation TVSContactCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configureWithContact:(TVSContact*)contact{
    self.textLabel.text = contact.name;
}

@end
