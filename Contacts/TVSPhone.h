//
//  TVSPhone.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TVSManagedObject.h"

@class TVSContact;

NS_ASSUME_NONNULL_BEGIN

@interface TVSPhone : TVSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "TVSPhone+CoreDataProperties.h"
