//
//  TVSEmail+CoreDataProperties.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSEmail+CoreDataProperties.h"

@implementation TVSEmail (CoreDataProperties)

@dynamic address;
@dynamic label;
@dynamic contact;

@end
