//
//  TVSAddress.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSAddress.h"
#import "TVSContact.h"
#import "TVSDataController.h"

@implementation TVSAddress

// Insert code here to add functionality to your managed object subclass

+(NSSet*)setFromArray:(NSArray *)array{
    NSMutableSet *set = [NSMutableSet new];
    for(NSDictionary *dictionary in array)
    {
        TVSAddress *address = [TVSAddress findOrCreateWithUniqueValue:dictionary[@"address1"] forKey:@"address1" inManagedObjectContext:[TVSDataController sharedInstance].mainManagedObjectContext];
        [address updateWithResponseDictionary:dictionary];
        [set addObject:address];
    }
    return set;
}

-(void)updateWithResponseDictionary:(NSDictionary *)dictionary{
    self.address2 = dictionary[@"address2"];
    self.address3 = dictionary[@"address3"];
    self.zipCode = [dictionary[@"zipcode"] stringValue];
    self.city = dictionary[@"city"];
    self.state = dictionary[@"state"];
    self.country = dictionary[@"country"];
}
@end
