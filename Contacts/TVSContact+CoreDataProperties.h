//
//  TVSContact+CoreDataProperties.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSContact.h"

NS_ASSUME_NONNULL_BEGIN

@interface TVSContact (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *imageUrl;
@property (nullable, nonatomic, retain) NSString *homePage;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *position;
@property (nullable, nonatomic, retain) NSNumber *age;
@property (nullable, nonatomic, retain) TVSCompany *company;
@property (nullable, nonatomic, retain) NSSet<TVSAddress *> *addresses;
@property (nullable, nonatomic, retain) NSSet<TVSEmail *> *emails;
@property (nullable, nonatomic, retain) NSSet<TVSPhone *> *phones;

@end

@interface TVSContact (CoreDataGeneratedAccessors)

- (void)addAddressesObject:(TVSAddress *)value;
- (void)removeAddressesObject:(TVSAddress *)value;
- (void)addAddresses:(NSSet<TVSAddress *> *)values;
- (void)removeAddresses:(NSSet<TVSAddress *> *)values;

- (void)addEmailsObject:(TVSEmail *)value;
- (void)removeEmailsObject:(TVSEmail *)value;
- (void)addEmails:(NSSet<TVSEmail *> *)values;
- (void)removeEmails:(NSSet<TVSEmail *> *)values;

- (void)addPhonesObject:(TVSPhone *)value;
- (void)removePhonesObject:(TVSPhone *)value;
- (void)addPhones:(NSSet<TVSPhone *> *)values;
- (void)removePhones:(NSSet<TVSPhone *> *)values;

@end

NS_ASSUME_NONNULL_END
