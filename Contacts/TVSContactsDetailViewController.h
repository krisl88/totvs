//
//  TVSContactsDetailViewController.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TVSContact;

@interface TVSContactsDetailViewController : UIViewController

+(instancetype)contactsDetailViewControllerWithContact:(TVSContact*)contact;
@end
