//
//  TVSContactDetailCell.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSContactDetailCell.h"
#import "TVSContact.h"

@interface TVSContactDetailCell()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *informationLabel;

@end

@implementation TVSContactDetailCell

-(void)configureWithContact:(TVSContact *)contact{
    self.avatarImageView.image = nil; //NOTE: make network call to load contact.imageUrl into avatarImageView
    self.informationLabel.text = [NSString stringWithFormat:@"%@ (%@)\n%@\n%@", contact.name, contact.age.stringValue, contact.position, contact.homePage];
}


@end
