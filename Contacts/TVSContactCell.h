//
//  TVSContactCell.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TVSContact;

@interface TVSContactCell : UITableViewCell

-(void)configureWithContact:(TVSContact*)contact;
@end
