//
//  TVSContactsDetailViewController.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSContactsDetailViewController.h"
#import "TVSContact.h"
#import "TVSContactCell.h"

@interface TVSContactsDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) TVSContact *contact;

@end

@implementation TVSContactsDetailViewController


+(instancetype)contactsDetailViewControllerWithContact:(TVSContact*)contact{
    TVSContactsDetailViewController *cdvc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"TVSContactsDetailViewController"];
    cdvc.contact = contact;
    return cdvc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.tableView.estimatedRowHeight = 160;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TVSContactCell* cell = [tableView dequeueReusableCellWithIdentifier:!indexPath.row ? @"TVSContactDetailCell" : @"TVSContactInfoCell"];
    [cell configureWithContact:self.contact];
    return cell;
}


@end
