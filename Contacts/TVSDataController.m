//
//  TVSDataController.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSDataController.h"


@interface TVSDataController ()
@property (nonatomic, strong, readwrite) NSString *storeType;
@property (nonatomic, strong, readwrite) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong, readwrite) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readwrite) NSManagedObjectContext *mainManagedObjectContext;
@end

@implementation TVSDataController

#pragma mark - Class Methods

+ (NSString *)managedObjectModelName;
{
    return @"Contacts";
}

+ (NSString *)storeName;
{
    return @"Contacts.sqlte";
}

+ (NSURL *)storeURL;
{
    return [[self _applicationDocumentsDirectory] URLByAppendingPathComponent:[self storeName]];
}

+ (NSURL *)_applicationDocumentsDirectory;
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Initialization

+ (instancetype)sharedInstance
{
    static TVSDataController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.storeType = NSSQLiteStoreType;
        [self loadControllerComponents];
    }
    
    return self;
}

- (void)dealloc;
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadControllerComponents;
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextDidSave:) name:NSManagedObjectContextDidSaveNotification object:nil];
    [self _loadManagedObjectModel];
    [self _loadPersistentStoreCoordinator];
    [self _loadMainManagedObjectContext];
}

#pragma mark - Public Methods

- (void)deleteAllData;
{
    NSError * error;
    NSURL * storeURL = [self.mainManagedObjectContext.persistentStoreCoordinator URLForPersistentStore:self.mainManagedObjectContext.persistentStoreCoordinator.persistentStores.lastObject];
    [self.mainManagedObjectContext lock];
    [self.mainManagedObjectContext reset];
    
    if ([[self.mainManagedObjectContext persistentStoreCoordinator] removePersistentStore:self.mainManagedObjectContext.persistentStoreCoordinator.persistentStores.lastObject error:&error]) {
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
        [[self.mainManagedObjectContext persistentStoreCoordinator] addPersistentStoreWithType:[self storeType] configuration:nil URL:storeURL options: @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    }
    [self.mainManagedObjectContext unlock];
}


#pragma mark - Core Data Saving support

- (void)contextDidSave:(NSNotification *)notification;
{
    @synchronized(self) {
        NSManagedObjectContext *notificationContext = notification.object;
        BOOL sameContext = [self.mainManagedObjectContext isEqual:notificationContext];
        BOOL samePersistentStoreCoordinator = [self.mainManagedObjectContext.persistentStoreCoordinator isEqual:notificationContext.persistentStoreCoordinator];
        if (!sameContext && samePersistentStoreCoordinator) {
            [self.mainManagedObjectContext mergeChangesFromContextDidSaveNotification:notification];
        }
    }
}

- (void)saveContext;
{
    NSManagedObjectContext *managedObjectContext = self.mainManagedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        }
    }
}

#pragma mark - Private methods

- (void)_loadPersistentStoreCoordinator;
{
    // Create the coordinator and store
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
    NSURL *storeURL = [[self class] storeURL];
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    while (![_persistentStoreCoordinator addPersistentStoreWithType:self.storeType configuration:nil URL:storeURL options:options error:&error]) {
#ifdef DEBUG
        NSLog(@"Error loading the peristent store, now deleting everything. %@, %@", error, [error userInfo]);
#endif
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:NULL];
    }
}

- (void)_loadMainManagedObjectContext;
{
    self.mainManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [self.mainManagedObjectContext setPersistentStoreCoordinator:self.persistentStoreCoordinator];
}

- (void)_loadManagedObjectModel;
{
    NSURL *modelURL = [[NSBundle bundleForClass:[self class]] URLForResource:[[self class] managedObjectModelName] withExtension:@"momd"];
    self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
}


@end

