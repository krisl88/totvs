//
//  TVSManagedObject.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface TVSManagedObject : NSManagedObject
@property (nonatomic, copy, readonly) NSString *uid;

+ (instancetype)insertIntoManagedObjectContext:(NSManagedObjectContext *)context;

// Find
+ (instancetype)findWithUniqueValue:(id)value forKey:(NSString *)key inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSArray *)findAllWithUniqueValues:(NSArray *)values forKey:(NSString *)key inManagedObjectContext:(NSManagedObjectContext *)context;

// Find and create
+ (instancetype)findOrCreateWithUniqueValue:(id)value forKey:(NSString *)key inManagedObjectContext:(NSManagedObjectContext *)context;
+ (instancetype)findOrCreateWithUniqueValues:(NSArray*)values forKeys:(NSArray *)keys inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSFetchRequest *)fetchRequestInManagedObjectContext:(NSManagedObjectContext *)context;

+ (NSArray *) allObjects:(NSManagedObjectContext*)context;

- (void)updateWithResponseDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)requestDictionary;
+(NSSet*)setFromArray:(NSArray*)array;
@end
