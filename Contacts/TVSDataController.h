//
//  TVSDataController.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface TVSDataController : NSObject

@property (nonatomic, strong, readonly) NSManagedObjectContext *mainManagedObjectContext;


+(TVSDataController*)sharedInstance;

-(void)saveContext;
- (void)deleteAllData;

@end
