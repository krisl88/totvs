//
//  TVSContact.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSContact.h"
#import "TVSAddress.h"
#import "TVSCompany.h"
#import "TVSEmail.h"
#import "TVSPhone.h"

@implementation TVSContact

// Insert code here to add functionality to your managed object subclass

-(void)updateWithResponseDictionary:(NSDictionary *)dictionary{
    self.age = dictionary[@"age"];
    self.position = dictionary[@"position"];
    self.homePage = dictionary[@"homePage"];
    self.imageUrl = dictionary[@"imageUrl"];
    
    self.company = [TVSCompany findOrCreateWithUniqueValue:dictionary[@"companyDetails"][@"name"] forKey:@"name" inManagedObjectContext:self.managedObjectContext];
    [self.company updateWithResponseDictionary:dictionary[@"companyDetails"]];
    self.company.contact = self;
    
    self.addresses = [TVSAddress setFromArray:dictionary[@"address"]];
    for(TVSAddress *address in self.addresses)
        address.contact = self;
    
    self.emails = [TVSEmail setFromArray:dictionary[@"email"]];
    for(TVSEmail *email in self.emails)
        email.contact = self;
    
    self.phones = [TVSPhone setFromArray:dictionary[@"phones"]];
    for(TVSPhone *phone in self.phones)
        phone.contact = self;
}
@end
