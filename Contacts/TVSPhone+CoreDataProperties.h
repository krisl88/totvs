//
//  TVSPhone+CoreDataProperties.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSPhone.h"

NS_ASSUME_NONNULL_BEGIN

@interface TVSPhone (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *number;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) TVSContact *contact;

@end

NS_ASSUME_NONNULL_END
