//
//  TVSContactInfoCell.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSContactInfoCell.h"
#import "TVSContact.h"
#import "TVSCompany.h"
#import "TVSEmail.h"
#import "TVSPhone.h"
#import "TVSAddress.h"

@interface TVSContactInfoCell()

@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end


@implementation TVSContactInfoCell

-(void)configureWithContact:(TVSContact *)contact{
    NSMutableString *mstr = [[NSMutableString alloc] init];
    
    //NOTE: the sample json has duplicate emails, phones, and addresses for all contacts
    //      this wasn't expected as I modeled them to have one to many relationships (instead of many-to-many) 
    if(contact.company)
    {
        [mstr appendString:[NSString stringWithFormat:@"%@\n",contact.company.name]];
        [mstr appendString:[NSString stringWithFormat:@"%@\n\n",contact.company.location]];
    }
    
    for(TVSAddress *address in contact.addresses)
    {
        [mstr appendString:[NSString stringWithFormat:@"%@\n",address.address1]];
        if(address.address2)
            [mstr appendString:[NSString stringWithFormat:@"%@\n",address.address2]];
        if(address.address3)
            [mstr appendString:[NSString stringWithFormat:@"%@\n",address.address3]];
        
        [mstr appendString:[NSString stringWithFormat:@"%@ %@ %@\n%@\n\n", address.city, address.state, address.zipCode, address.country]];
    }
    
    for(TVSEmail *email in contact.emails)
    {
        [mstr appendString:[NSString stringWithFormat:@"%@\n",email.address]];
        if(email.label)
            [mstr appendString:[NSString stringWithFormat:@"%@\n\n",email.label]];
    }

    for(TVSPhone *phone in contact.phones)
    {
        [mstr appendString:[NSString stringWithFormat:@"%@\n", phone.number]];
        if(phone.type)
            [mstr appendString:[NSString stringWithFormat:@"%@\n\n", phone.type]];
    }
    self.infoLabel.text = mstr;
}
@end
