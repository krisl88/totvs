//
//  TVSManagedObject.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSManagedObject.h"

@interface TVSManagedObject ()

@property (nonatomic, copy, readwrite) NSString *uid;

@end


@implementation TVSManagedObject

@dynamic uid;

#pragma mark - Class Methods

+ (instancetype)insertIntoManagedObjectContext:(NSManagedObjectContext *)context;
{
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityNameInManagedObjectContext:context] inManagedObjectContext:context];
}

+ (NSArray *)findAllWithUniqueValues:(NSArray *)values forKey:(NSString *)key inManagedObjectContext:(NSManagedObjectContext *)context;
{
    NSFetchRequest *fetchRequest = [self fetchRequestInManagedObjectContext:context];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K IN %@", key, values];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count) {
        return fetchedObjects;
    } else {
        return nil;
    }
}

+ (instancetype)findWithUniqueValue:(id)value forKey:(NSString *)key inManagedObjectContext:(NSManagedObjectContext *)context
{
    NSFetchRequest *fetchRequest = [self fetchRequestInManagedObjectContext:context];
    fetchRequest.fetchLimit = 1;
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K = %@", key, value];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count) {
        return fetchedObjects.firstObject;
    } else {
        return nil;
    }
}

+ (instancetype)findOrCreateWithUniqueValue:(id)value forKey:(NSString *)key inManagedObjectContext:(NSManagedObjectContext *)context{
    TVSManagedObject *managedObject = [self findWithUniqueValue:value forKey:key inManagedObjectContext:context];
    
    if (!managedObject) {
        managedObject = [self insertIntoManagedObjectContext:context];
        [managedObject setValue:value forKey:key];
    }
    
    return managedObject;
}


+ (instancetype)findOrCreateWithUniqueValues:(NSArray*)values forKeys:(NSArray *)keys inManagedObjectContext:(NSManagedObjectContext *)context{
    
    if(values.count != keys.count)
    {
        return nil;
    }
    
    NSFetchRequest *fetchRequest = [self fetchRequestInManagedObjectContext:context];
    fetchRequest.fetchLimit = 1;
    NSMutableArray *predicates = [NSMutableArray new];
    for(int i = 0; i < values.count; i++)
    {
        [predicates addObject:[NSPredicate predicateWithFormat:@"%K = %@", keys[i], values[i]]];
    }
    fetchRequest.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count) {
        return fetchedObjects.firstObject;
    }
    
    TVSManagedObject *managedObject = [self insertIntoManagedObjectContext:context];
    
    for(int i = 0; i < values.count; i++)
    {
        id value = values[i];
        NSString *key = keys[i];
        [managedObject setValue:value forKey:key];
    }
    return managedObject;
}

+ (NSArray *) allObjects:(NSManagedObjectContext*)context
{
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:[self fetchRequestInManagedObjectContext:context] error:&error];
    if (error != nil)
        return nil;
    
    return results;
}

+ (NSFetchRequest *)fetchRequestInManagedObjectContext:(NSManagedObjectContext *)context
{
    return [NSFetchRequest fetchRequestWithEntityName:[self entityNameInManagedObjectContext:context]];
}

+ (NSString *)entityNameInManagedObjectContext:(NSManagedObjectContext *)context
{
    NSString *myName = NSStringFromClass(self);
    NSManagedObjectModel *model = context.persistentStoreCoordinator.managedObjectModel;
    for (NSEntityDescription *description in model.entities) {
        if ([description.managedObjectClassName isEqualToString:myName]) {
            return description.name;
            break;
        }
    }
    
    return nil;
}

- (void)updateWithResponseDictionary:(NSDictionary *)dictionary
{
    
}

- (NSDictionary *)requestDictionary;
{
    
    return nil;
}

+(NSSet*)setFromArray:(NSArray *)array{
    return nil;
}

@end