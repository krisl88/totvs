//
//  TVSContactsViewController.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "SVProgressHUD.h"
#import "TVSContactsViewController.h"
#import "TVSContactController.h"
#import "TVSContact.h"
#import "TVSDataController.h"
#import "TVSContactCell.h"
#import "TVSContactsDetailViewController.h"

@interface TVSContactsViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSFetchedResultsController *contactsFRC;
@end

@implementation TVSContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [SVProgressHUD show];
    [[TVSContactController sharedInstance] fetchContactsWithCompletion:^(NSArray *mappedObjects, NSError *error) {
        [self setupFRC];
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
    }];
}

-(void)setupFRC{
    NSFetchRequest *fetchRequest = [TVSContact fetchRequestInManagedObjectContext:[TVSDataController sharedInstance].mainManagedObjectContext];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
    self.contactsFRC = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[TVSDataController sharedInstance].mainManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
    [self.contactsFRC performFetch:NULL];
}


#pragma mark - search delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    //NOTE: only searching for name matches
    self.contactsFRC.fetchRequest.predicate = searchText.length > 0 ? [NSPredicate predicateWithFormat:@"name contains[cd] %@", searchText] : nil;
    [self.contactsFRC performFetch:NULL];
    [self.tableView reloadData];
}

- (IBAction)moreButtonPressed:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:ok];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contactsFRC.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TVSContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TVSContactCell"];
    [cell configureWithContact:self.contactsFRC.fetchedObjects[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TVSContactsDetailViewController *tdvc = [TVSContactsDetailViewController contactsDetailViewControllerWithContact:self.contactsFRC.fetchedObjects[indexPath.row]];
    [self.navigationController pushViewController:tdvc animated:YES];
    
}


@end
