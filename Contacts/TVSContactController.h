//
//  TVSContactController.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TVSNetworkController.h"

@interface TVSContactController : NSObject

+(TVSContactController*)sharedInstance;

-(void)fetchContactsWithCompletion:(TVSArrayCompletionBlock)completion;
@end
