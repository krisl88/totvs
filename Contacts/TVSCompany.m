//
//  TVSCompany.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSCompany.h"
#import "TVSContact.h"

@implementation TVSCompany

-(void)updateWithResponseDictionary:(NSDictionary *)dictionary{
    self.location = dictionary[@"location"];
}
@end
