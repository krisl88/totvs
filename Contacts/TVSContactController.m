//
//  TVSContactController.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSContactController.h"
#import "TVSContact.h"
#import "TVSDataController.h"
@implementation TVSContactController


+ (instancetype)sharedInstance
{
    static TVSContactController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)fetchContactsWithCompletion:(TVSArrayCompletionBlock)completion{
    NSString *path = @"http://beta.json-generator.com/api/json/get/4yLVmeGYe";
    [[TVSNetworkController sharedInstance] getRequestWithPath:path completionBlock:^(id responseObject, NSError *error) {
        
        NSArray *contacts = [self contactsForResponseDictionary:responseObject];
        [[TVSDataController sharedInstance] saveContext];
       if(completion)
            completion(contacts, error);
    }];
}

-(NSArray*)contactsForResponseDictionary:(NSArray*)responseArray{
    NSMutableArray* contacts = [NSMutableArray new];
    
    for(NSDictionary *dictionary in responseArray)
    {
        //NOTE: in the absence of an unique id, like a contact guid, making an assumption name is unique (bad assumption!)
        TVSContact *contact = [TVSContact findOrCreateWithUniqueValue:dictionary[@"name"] forKey:@"name" inManagedObjectContext:[TVSDataController sharedInstance].mainManagedObjectContext];
        [contact updateWithResponseDictionary:dictionary];
        [contacts addObject:contact];
    }
    return contacts;
}
@end
