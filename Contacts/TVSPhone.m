//
//  TVSPhone.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//

#import "TVSPhone.h"
#import "TVSContact.h"
#import "TVSDataController.h"

@implementation TVSPhone

// Insert code here to add functionality to your managed object subclass
+(NSSet*)setFromArray:(NSArray *)array{
    NSMutableSet *set = [NSMutableSet new];
    for(NSDictionary *dictionary in array)
    {
        TVSPhone *phone = [TVSPhone findOrCreateWithUniqueValue:dictionary[@"number"] forKey:@"number" inManagedObjectContext:[TVSDataController sharedInstance].mainManagedObjectContext];
        [phone updateWithResponseDictionary:dictionary];
        [set addObject:phone];
    }
    return set;
}

-(void)type:(NSDictionary *)dictionary{
    self.type = dictionary[@"label"];
}

@end
