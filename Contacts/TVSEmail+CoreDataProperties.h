//
//  TVSEmail+CoreDataProperties.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSEmail.h"

NS_ASSUME_NONNULL_BEGIN

@interface TVSEmail (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) NSString *label;
@property (nullable, nonatomic, retain) TVSContact *contact;

@end

NS_ASSUME_NONNULL_END
