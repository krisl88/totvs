//
//  TVSAddress+CoreDataProperties.h
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSAddress.h"

NS_ASSUME_NONNULL_BEGIN

@interface TVSAddress (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address1;
@property (nullable, nonatomic, retain) NSString *address2;
@property (nullable, nonatomic, retain) NSString *address3;
@property (nullable, nonatomic, retain) NSString *zipCode;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *state;
@property (nullable, nonatomic, retain) NSString *country;
@property (nullable, nonatomic, retain) TVSContact *contact;

@end

NS_ASSUME_NONNULL_END
