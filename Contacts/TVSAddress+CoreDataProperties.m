//
//  TVSAddress+CoreDataProperties.m
//  Contacts
//
//  Created by Kris Lau on 1/28/16.
//  Copyright © 2016 Kris Lau. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TVSAddress+CoreDataProperties.h"

@implementation TVSAddress (CoreDataProperties)

@dynamic address1;
@dynamic address2;
@dynamic address3;
@dynamic zipCode;
@dynamic city;
@dynamic state;
@dynamic country;
@dynamic contact;

@end
